/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tecsup.security.dao;

import com.tecsup.security.model.Usuario;

/**
 *
 * @author César Pantoja
 */
public interface UsuarioDAO{

 Usuario findByLogin(String usuario);
}