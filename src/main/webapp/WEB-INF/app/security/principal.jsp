<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
    <body>
        <h1>Bienvenido a la Tienda de Comercio Electrónico</h1>
        <h2>${mensaje}</h2>
        <h4><a href="<%=request.getContextPath()%>/vendedor/principal">Vendedor</a></h4>
        <h4><a href="<%=request.getContextPath()%>/adm/principal">Administrador</a></h4>
         <h4><a href="<%=request.getContextPath()%>/vendedor/productos">Catalogo Producto</a></h4>

    <sec:authorize access="isAnonymous()">
        <a href="<%=request.getContextPath()%>/login">Login</a>            
    </sec:authorize>
    
    <sec:authorize access="isAuthenticated()">
        <a href="<%=request.getContextPath()%>/logout">Logout</a>            
    </sec:authorize>
    
    
</body>
</html>